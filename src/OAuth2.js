import Model from './Model';

export default class OAuth2 extends Model {
  constructor(keys = {}) {
    super({
      code: 'code',
      access_token: 'access_token',
      access_token_expires_at: 'access_token_expires_at',
      refresh_token: 'refresh_token',
      refresh_token_expires_at: 'refresh_token_expires_at',
      user: 'user',
      client: 'client',
      ...keys
    });
  }

  initialize(model) {
    this._model = model;
  }

  handleError() {
    if(typeof this._model.handleError === 'function')
      this._model.handleError.call(this, ...arguments);
  }

  createCode() {
    return typeof this._model.createCode === 'function'
      ? this._model.createCode.call(this, ...arguments)
      : this._defaultCode();
  }

  createCodeToken() {
    return typeof this._model.createCodeToken === 'function'
      ? this._model.createCodeToken.call(this, ...arguments)
      : this._defaultToken();
  }

  createPasswordToken() {
    return typeof this._model.createPasswordToken === 'function'
      ? this._model.createPasswordToken.call(this, ...arguments)
      : this._defaultToken();
  }

  refreshToken() {
    return typeof this._model.refreshToken === 'function'
      ? this._model.refreshToken.call(this, ...arguments)
      : this._defaultToken();
  }

  authorizeAsync(flow, args = []) {
    switch(flow) {
      case 'authorization_code':
        return this._authorizationCode(args);
      case 'password':
        return this._password(args);
      default:
        throw new Error(`Invalid argument '${flow}'`);
    }
  }

  async getTokenAsync() {
    const oauth2 = this.getToken();

    const token = oauth2
      ? JSON.parse(oauth2)
      : await this._defaultToken();

    if(
      token[this._keys.access_token]
      && token[this._keys.access_token_expires_at]
      && !this._expired(token[this._keys.access_token_expires_at])
    ) return token;

    const newtoken = await this.refreshToken(token[this._keys.refresh_token]);

    if(!newtoken) return this._defaultToken();

    this.saveToken(newtoken);

    return newtoken;
  }

  signout() {
    this.clearToken();

    if(typeof this._model.signout === 'function')
      this._model.signout.call(this, ...arguments);
  }
}

